==========
hv-helpers
==========


.. image:: https://img.shields.io/pypi/v/hv_helpers.svg
        :target: https://pypi.python.org/pypi/hv_helpers

.. image:: https://img.shields.io/travis/nluetts/hv_helpers.svg
        :target: https://travis-ci.com/nluetts/hv_helpers

.. image:: https://readthedocs.org/projects/hv-helpers/badge/?version=latest
        :target: https://hv-helpers.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Helper functions for holoviews


* Free software: MIT license
* Documentation: https://hv-helpers.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
