# %load /home/jovyan/snippets/hv_linfit.py
import holoviews as hv
import numpy as np

from holoviews.operation import Operation


class hv_linfit(Operation):
    """
    Calculate linear fit and plot the result.

    Numeric fitting results (parameters and covariance matrix)
    are stored in the .meta dictionary of the returned overlay.
    """

    def _process(self, el, key=None):

        x = el.dimension_values(0)
        y = el.dimension_values(1)
        fit = np.polyfit(x, y, 1, cov=True)
        std_err = np.sqrt(np.diag(fit[1]))

        span = max(x) - min(x)
        mean = (max(x) + min(x))/2
        x_fit = [mean - span*0.55, mean + span*0.55]
        y_fit = np.polyval(fit[0], x_fit)
        res = y - np.polyval(fit[0], x)

        fit_plot = el.clone((x_fit, y_fit)).to(hv.Curve, label="fit")
        residual_plot = el.clone((x, res), label="residual", vdims="residuals")
        residual_plot.opts(height=250)
        layout = (fit_plot * el + residual_plot).cols(1)
        layout.meta = dict(coeff=fit[0], se=std_err, cov=fit[1])

        return layout

