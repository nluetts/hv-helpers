import holoviews as hv


def hv_pick_peaks(element, dimensions=None, marker_opts=None):
    """Draw canvas that allows to manually select points."""
    if marker_opts is None:
        marker_opts = dict(size=25, color="red", alpha=0.5)
    if dimensions is None:
        dimensions = [d.name for d in element.dimensions()]

    canvas = hv.Scatter(([], []), kdims=dimensions[0], vdims=dimensions[1])
    canvas.opts(marker_opts)
    marker_stream = hv.streams.PointDraw(source=canvas)

    return marker_stream, element * canvas
