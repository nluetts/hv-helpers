import numpy as np

from holoviews.operation import Operation
from scipy.spatial import ConvexHull

# https://dsp.stackexchange.com/questions/2725/how-to-perform-a-rubberband-correction-on-spectroscopic-data

class rubberband(Operation):
    """Rubberband baseline correction using convex hull."""
    def _process(self, el, key=None):
        x = el.dimension_values(0)
        y = el.dimension_values(1)
        # find the convex hull
        v = ConvexHull(np.array(list(zip(x, y)))).vertices
        # Rotate convex hull vertices until they start from the lowest one
        v = np.roll(v, -v.argmin())
        # Leave only the ascending part
        v = v[:v.argmax()]

        # Create baseline using linear interpolation between vertices
        y_corr = np.interp(x, x[v], y[v])
        return el.clone((x, y - y_corr))