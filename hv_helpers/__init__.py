"""Top-level package for hv-helpers."""

__author__ = """Nils Luettschwager"""
__email__ = 'nluetts@gwdg.de'
__version__ = '0.1.0'

"""Main module."""


from .hv_curvefit import hv_curvefit
from .hv_linfit import hv_linfit
from .hv_normalize import hv_normalize
from .hv_peak_picking import find_peaks
from .hv_pick_peaks import hv_pick_peaks
from .hv_rubberband import rubberband


__all__ = [
    "hv_curvefit",
    "hv_linfit",
    "hv_normalize",
    "find_peaks",
    "hv_pick_peaks",
    "rubberband"
]