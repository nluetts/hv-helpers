import param
import holoviews as hv

from holoviews.operation import Operation
from scipy.signal import resample

class upscale(Operation):

    factor = param.Number(default=10)
    kw = param.Dict(default=dict())
    
    def _process(self, el, key=None):
        xvals = el.dimension_values(0)
        yvals = el.dimension_values(1)
        yfine, xfine = resample(
            yvals,
            self.p.factor * xvals.size,
            t = xvals,
            **self.p.kw
        )
        return el.clone((xfine, yfine))
