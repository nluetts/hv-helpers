import holoviews as hv
import numpy as np
import param

from holoviews.operation import Operation
from scipy.optimize import curve_fit


class hv_curvefit(Operation):
    """
    Calculate fit of function `func` and plot the result.
    
    Numeric fitting results (parameters and covariance matrix)
    are stored in the .meta dictionary of the returned overlay.
    """
    
    func = param.Callable(doc="fitting funcion")
    
    def _process(self, el, key=None):

        x = el.dimension_values(0)
        y = el.dimension_values(1)

        fit = curve_fit(self.p.func, x, y)
        
        span = max(x) - min(x)
        mean = (max(x) + min(x))/2
        x_fit = np.linspace(mean - span*0.55, mean + span*0.55, 100)
        y_fit = self.p.func(x_fit, *fit[0])
        res = y - self.p.func(x, *fit[0])
        
        fit_plot = el.clone((x_fit, y_fit)).to(hv.Curve, label="fit")
        residual_plot = el.clone((x, res), label="residual", vdims="residuals")
        residual_plot.opts(height=250)
        layout = (fit_plot * el + residual_plot).cols(1)
        layout.meta = dict(fit_results=fit)
        
        return layout