import param
from holoviews.operation import Operation


class hv_normalize(Operation):
    """Normalize data to lower +/- span/2."""
    lower = param.Number(default=None)
    span = param.Number(default=None)

    def _process(self, el, key=None):
        xvals = el.dimension_values(0)
        yvals = el.dimension_values(1)
        lo = self.p.lower if self.p.lower else yvals.min()
        sp = self.p.span if self.p.span else yvals.ptp()
        yvals = (yvals-lo)/sp
        return el.clone((xvals, yvals))