import param

from holoviews.operation import Operation
from scipy.signal import find_peaks as scipy_find_peaks


class find_peaks(Operation):

    height = param.Number(default=0)
    distance = param.Integer(default=1)
    width = param.NumericTuple(default=None, length=2)
    prominence = param.Number(default=None)

    def _process(self, el, key=None):
        xvals = el.dimension_values(0)
        yvals = el.dimension_values(1)
        idx = scipy_find_peaks(
            yvals,
            height=self.p.height,
            distance=self.p.distance,
            width=self.p.width,
            prominence=self.p.prominence,
        )[0]
        peaks = el.clone((xvals[idx], yvals[idx])).to(hv.Scatter)
        return peaks
